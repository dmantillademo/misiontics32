-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: eventosapp
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `evp_cargoadicional`
--

DROP TABLE IF EXISTS `evp_cargoadicional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evp_cargoadicional` (
  `idevento` int NOT NULL,
  `idservicio` int NOT NULL,
  `valor` decimal(9,2) NOT NULL,
  PRIMARY KEY (`idservicio`,`idevento`),
  KEY `fk_evp_cargoadicional_evp_evento1_idx` (`idevento`),
  KEY `fk_evp_cargoadicional_evp_servicio1_idx` (`idservicio`),
  CONSTRAINT `fk_evp_cargoadicional_evp_evento1` FOREIGN KEY (`idevento`) REFERENCES `evp_evento` (`idevento`),
  CONSTRAINT `fk_evp_cargoadicional_evp_servicio1` FOREIGN KEY (`idservicio`) REFERENCES `evp_servicio` (`idservicio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evp_cargoadicional`
--

LOCK TABLES `evp_cargoadicional` WRITE;
/*!40000 ALTER TABLE `evp_cargoadicional` DISABLE KEYS */;
/*!40000 ALTER TABLE `evp_cargoadicional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evp_cliente`
--

DROP TABLE IF EXISTS `evp_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evp_cliente` (
  `idcliente` int NOT NULL AUTO_INCREMENT,
  `documento` varchar(45) NOT NULL,
  `tipodoc` varchar(2) NOT NULL,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `celular` varchar(10) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcliente`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evp_cliente`
--

LOCK TABLES `evp_cliente` WRITE;
/*!40000 ALTER TABLE `evp_cliente` DISABLE KEYS */;
INSERT INTO `evp_cliente` VALUES (1,'1098645322','CC','Diana','Afanador','3106303417','',NULL);
/*!40000 ALTER TABLE `evp_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evp_evento`
--

DROP TABLE IF EXISTS `evp_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evp_evento` (
  `idevento` int NOT NULL AUTO_INCREMENT,
  `idcliente` int NOT NULL,
  `idsalon` int NOT NULL,
  `fecha` date NOT NULL,
  `hora_inicial` int NOT NULL,
  `hora_final` int NOT NULL,
  `vr_total` decimal(9,2) NOT NULL,
  `observaciones` text,
  PRIMARY KEY (`idevento`,`idcliente`,`idsalon`),
  KEY `fk_evp_evento_evp_cliente_idx` (`idcliente`),
  KEY `fk_evp_evento_evp_salon1_idx` (`idsalon`),
  CONSTRAINT `fk_evp_evento_evp_cliente` FOREIGN KEY (`idcliente`) REFERENCES `evp_cliente` (`idcliente`),
  CONSTRAINT `fk_evp_evento_evp_salon1` FOREIGN KEY (`idsalon`) REFERENCES `evp_salon` (`idsalon`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evp_evento`
--

LOCK TABLES `evp_evento` WRITE;
/*!40000 ALTER TABLE `evp_evento` DISABLE KEYS */;
INSERT INTO `evp_evento` VALUES (1,1,1,'2021-01-30',8,9,150000.00,NULL);
/*!40000 ALTER TABLE `evp_evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evp_salon`
--

DROP TABLE IF EXISTS `evp_salon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evp_salon` (
  `idsalon` int NOT NULL AUTO_INCREMENT,
  `salon` varchar(45) NOT NULL,
  `descripcion` text NOT NULL,
  `direccion` text NOT NULL,
  `valor_hora` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`idsalon`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evp_salon`
--

LOCK TABLES `evp_salon` WRITE;
/*!40000 ALTER TABLE `evp_salon` DISABLE KEYS */;
INSERT INTO `evp_salon` VALUES (1,'T01-01','Salon ubicado en la torre A, Piso 1','Carrera 20 # 3 34',250000.00);
/*!40000 ALTER TABLE `evp_salon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evp_servicio`
--

DROP TABLE IF EXISTS `evp_servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evp_servicio` (
  `idservicio` int NOT NULL AUTO_INCREMENT,
  `servicio` varchar(45) NOT NULL,
  `vr_servicio` decimal(9,2) NOT NULL,
  PRIMARY KEY (`idservicio`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evp_servicio`
--

LOCK TABLES `evp_servicio` WRITE;
/*!40000 ALTER TABLE `evp_servicio` DISABLE KEYS */;
INSERT INTO `evp_servicio` VALUES (1,'Mariachis',150000.00);
/*!40000 ALTER TABLE `evp_servicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_config` (
  `variable` varchar(128) NOT NULL,
  `value` varchar(128) DEFAULT NULL,
  `set_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `set_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`variable`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-12 13:38:38
