/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.modelo;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author DIEGO-PC
 */
public class Reserva {
    private Integer idevento;
    private Cliente cliente;
    private ArrayList<Servicio> servicios;
    private Salon salon;
    private Date fecha;
    private Integer hora_inicial;
    private Integer hora_final;
    private Double valor_total;
    private String observaciones;

    
/*    @Override
    public String toString()
    {
        return "Salon{idsalon=" + idsalon + ", salon=" + salon + ", descripcion=" + descripcion + ", direccion=" + direccion;
    }*/

    public Reserva() {
    }

    public Reserva(Integer idevento, Cliente cliente, ArrayList<Servicio> servicios, Salon salon, Date fecha, Integer hora_inicial, Integer hora_final, Double valor_total, String observaciones) {
        this.idevento = idevento;
        this.cliente = cliente;
        this.servicios = servicios;
        this.salon = salon;
        this.fecha = fecha;
        this.hora_inicial = hora_inicial;
        this.hora_final = hora_final;
        this.valor_total = valor_total;
        this.observaciones = observaciones;
    }

    public Integer getIdevento() {
        return idevento;
    }

    public void setIdevento(Integer idevento) {
        this.idevento = idevento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ArrayList<Servicio> getServicios() {
        return servicios;
    }

    public void setServicios(ArrayList<Servicio> servicios) {
        this.servicios = servicios;
    }

    public Salon getSalon() {
        return salon;
    }

    public void setSalon(Salon salon) {
        this.salon = salon;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getHora_inicial() {
        return hora_inicial;
    }

    public void setHora_inicial(Integer hora_inicial) {
        this.hora_inicial = hora_inicial;
    }

    public Integer getHora_final() {
        return hora_final;
    }

    public void setHora_final(Integer hora_final) {
        this.hora_final = hora_final;
    }

    public Double getValor_total() {
        return valor_total;
    }

    public void setValor_total(Double valor_total) {
        this.valor_total = valor_total;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    
    
}
