/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.modelo;

/**
 *
 * @author DIEGO-PC
 */
public class Salon {
    private Integer idsalon;
    private String salon;
    private String descripcion;
    private String direccion;
    private double valor_hora;

    public Salon() {
    }

    public Salon(Integer idsalon, String salon, String descripcion, String direccion, double valor_hora) {
        this.idsalon = idsalon;
        this.salon = salon;
        this.descripcion = descripcion;
        this.direccion = direccion;
        this.valor_hora = valor_hora;
    }

    public Integer getIdsalon() {
        return idsalon;
    }

    public void setIdsalon(Integer idsalon) {
        this.idsalon = idsalon;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getValor_hora() {
        return valor_hora;
    }

    public void setValor_hora(double valor_hora) {
        this.valor_hora = valor_hora;
    }
    
    @Override
    public String toString()
    {
        return "Salon{idsalon=" + idsalon + ", salon=" + salon + ", descripcion=" + descripcion + ", direccion=" + direccion;
    }
}
