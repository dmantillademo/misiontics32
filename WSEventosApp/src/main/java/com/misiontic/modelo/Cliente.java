/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.modelo;

/**
 *
 * @author DIEGO-PC
 */
public class Cliente {
    private Integer idcliente;
    private String documento;
    private String tipodoc;
    private String nombres;
    private String apellidos;
    private String telefono;
    private String celular;
    private String email;

    public Cliente() {
    }

    public Cliente(Integer idcliente, String documento, String tipodoc, String nombres, String apellidos, String telefono, String celular, String email) {
        this.idcliente = idcliente;
        this.documento = documento;
        this.tipodoc = tipodoc;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono = telefono;
        this.celular = celular;
        this.email = email;
    }

    public Integer getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Integer idcliente) {
        this.idcliente = idcliente;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString()
    {
        String idcliente= "idcliente = " + this.idcliente;
        String documento= "documento = " + this.documento;
        String tipodoc= "tipodoc = " + this.tipodoc;
        String nombres= "nombres = " + this.nombres;
        String apellidos= "apellidos = " + this.apellidos;
        String telefono= "telefono = " + this.telefono;
        String celular= "celular = " + this.celular;
        String email= "email = " + this.email;
        
        return "Cliente{" + idcliente + ", " + documento + ", " + tipodoc + ", " + nombres + ", " + apellidos + ", " + telefono + ", " + celular + ", " + email + "}";
    }
}
