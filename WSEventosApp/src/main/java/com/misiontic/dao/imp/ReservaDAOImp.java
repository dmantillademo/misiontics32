package com.misiontic.dao.imp;

import com.misiontic.dao.ClienteDAO;
import com.misiontic.dao.ReservaDAO;
import com.misiontic.dao.SqlCierre;
import com.misiontic.modelo.Cliente;
import com.misiontic.modelo.Reserva;
import com.misiontic.utilitario.Paginacion;
import com.misiontic.utilitario.RegistrosCrud;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import javax.sql.DataSource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DIEGO-PC
 */
public class ReservaDAOImp implements ReservaDAO {

private static final Logger LOG = Logger.getLogger(ReservaDAOImp.class.getName());
    private final DataSource pool;

    public ReservaDAOImp(DataSource pool) {
        this.pool = pool;
    }

    @Override
    public Paginacion getLista(HashMap<String, Object> parameters, Connection conn) throws SQLException {
        Paginacion paginacion = new Paginacion();
        List<Reserva> list = new ArrayList<>();
        PreparedStatement pst;
        ResultSet rs;
        try {
            pst = conn.prepareStatement("SELECT COUNT(idevento) AS COUNT FROM evp_evento WHERE "
                    + "idcliente LIKE CONCAT('%',?,'%')");
            pst.setString(1, String.valueOf(parameters.get("FILTER")));
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                paginacion.setFiltro_contador(rs.getInt("COUNT"));
                if (rs.getInt("COUNT") > 0) {
                    pst = conn.prepareStatement("SELECT * FROM evp_evento WHERE idcliente LIKE CONCAT('%',?,'%') "
                            + String.valueOf(parameters.get("SQL_ORDER_BY")) + " " + String.valueOf(parameters.get("SQL_PAGINATION")));
                    pst.setString(1, String.valueOf(parameters.get("FILTER")));
                    LOG.info(pst.toString());
                    rs = pst.executeQuery();
                    while (rs.next()) {
                        Reserva registro = new Reserva();
                        ClienteDAOImp cliente = new ClienteDAOImp(pool);
                        SalonDAOImp salon = new SalonDAOImp(pool);
                        registro.setCliente(cliente.getForId(rs.getLong("idcliente")));
                        registro.setSalon(salon.getForId(rs.getLong("idsalon")));
                        
                        registro.setIdevento(rs.getInt("idevento"));
                        registro.setFecha(rs.getDate("fecha"));
                        registro.setHora_inicial(rs.getInt("hora_inicial"));
                        registro.setHora_final(rs.getInt("hora_final"));
                        registro.setValor_total(rs.getDouble("vr_total"));
                        registro.setObservaciones(rs.getString("observaciones"));
                        list.add(registro);
                    }
                }
            }
            paginacion.setLista(list);
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return paginacion;
    }

    @Override
    public RegistrosCrud getLista(HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        try (Connection conn = this.pool.getConnection()) {
            crud.setPaginacion(getLista(parameters, conn));
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud agregar(Reserva t, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        PreparedStatement pst;
        ResultSet rs;
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            pst = conn.prepareStatement("SELECT COUNT(idcliente) AS COUNT FROM evp_cliente WHERE idevento = ?");
            pst.setInt(1, t.getIdevento());
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                if (rs.getInt("COUNT") == 0) {
                    //REALIZAMOS LA TRANSACCIÓN
                    pst = conn.prepareStatement("INSERT INTO evp_cliente(idcliente, idsalon, fecha, hora_inicial, hora_final, vr_total, observaciones) VALUES(?,?,?,?,?,?,?)");
                    pst.setInt(1, t.getCliente().getIdcliente());
                    pst.setInt(2, t.getSalon().getIdsalon());
                    pst.setDate(3, new Date(t.getFecha().getTime()));
                    pst.setInt(4, t.getHora_inicial());
                    pst.setInt(5, t.getHora_final());
                    pst.setDouble(6, t.getValor_total());
                    pst.setString(7, t.getObservaciones());
                    LOG.info(pst.toString());
                    pst.executeUpdate();
                    conn.commit();
                    crud.setMensaje_servidor("ok");
                    crud.setPaginacion(getLista(parameters, conn));
                } else {
                    //RECHAZAMOS EL REGISTRO
                    crud.setMensaje_servidor("No se registró, ya existe un Servicio con el nombre ingresado");
                }
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud editar(Reserva t, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        PreparedStatement pst;
        ResultSet rs;
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            pst = conn.prepareStatement("SELECT COUNT(idevento) AS COUNT FROM evp_evento WHERE idcliente = ? AND idevento != ?");
            pst.setInt(1, t.getCliente().getIdcliente());
            pst.setInt(2, t.getIdevento());
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                if (rs.getInt("COUNT") == 0) {
                    //REALIZAMOS LA TRANSACCIÓN
                    pst = conn.prepareStatement("UPDATE evp_evento SET idcliente = ?, idsalon = ?, fecha = ?, hora_inicial = ?, hora_final = ?, vr_total = ?, observaciones = ? WHERE idevento = ?");
                    pst.setInt(1, t.getCliente().getIdcliente());
                    pst.setInt(2, t.getSalon().getIdsalon());
                    pst.setDate(3, new Date(t.getFecha().getTime()));
                    pst.setInt(4, t.getHora_inicial());
                    pst.setInt(5, t.getHora_final());
                    pst.setDouble(6, t.getValor_total());
                    pst.setString(7, t.getObservaciones());
                    pst.setInt(8, t.getIdevento());
                    LOG.info(pst.toString());
                    pst.executeUpdate();
                    conn.commit();
                    crud.setMensaje_servidor("ok");
                    crud.setPaginacion(getLista(parameters, conn));
                } else {
                    //RECHAZAMOS EL REGISTRO
                    crud.setMensaje_servidor("No se modificó, ya existe un Servicio con el nombre ingresado");
                }
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud eliminar(Integer id, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud beanCrud = new RegistrosCrud();
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            try (PreparedStatement pst = conn.prepareStatement("DELETE FROM evp_evento WHERE idevento = ?")) {
                pst.setInt(1, id);
                LOG.info(pst.toString());
                pst.executeUpdate();
                conn.commit();
                beanCrud.setMensaje_servidor("ok");
                beanCrud.setPaginacion(getLista(parameters, conn));
            }
        } catch (SQLException e) {
            throw e;
        }
        return beanCrud;
    }

    @Override
    public Reserva getForId(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    

    
}
